## Synopsis

This is a simple 2D platformer game made in Unity for the [#novicenowjam](http://itch.io/jam/novices-now) on itch.io and gamejolt. This game is free to use for educational purposes. 

## Motivation

This game is a learning project. It's my first time using Unity, so be gentle :) 

## Contributors

This game was made by [mercurytw](mailto:mercurythewhite@gmail.com.)

## License

This game is released under the XXX License.
