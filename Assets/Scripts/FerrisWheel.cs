﻿/***
 * 
 * @copyright Copyright 2015 Jeff Cochran
 * 
 * This file is a part of the Jump Cat project.
 * 
 * All rights reserved.
 * 
 * @author mercurythewhite@gmail.com
 * 
 ***/

using UnityEngine;
using UnityEngine.Assertions;

public class FerrisWheel : MonoBehaviour {
    private const string kTag = "FerrisWheel: ";

    public float rot_velocity;
    public int num_spokes;

    private Vector3 euler_rotation = new Vector3();

    private System.Collections.ArrayList carriages;

    private void SetRotationVector2(ref Vector2 v, float rads) {
        float xprime, yprime, cosr, sinr;
        sinr = Mathf.Sin(rads);
        cosr = Mathf.Cos(rads);
        xprime = v.x * cosr - v.y * sinr;
        yprime = v.x * sinr + v.y * cosr;
        v.Set(xprime, yprime);
    }

    void Start () {
        Transform[] xforms = GetComponentsInChildren<Transform>();
        carriages = new System.Collections.ArrayList(num_spokes);

        Transform pivot = null;
        foreach ( Transform t in xforms ) {
            if (t == transform)
                continue;
            if (t.name.Equals("Pivot")) {
                pivot = t;
                break;
            }
        }

        Assert.IsNotNull(pivot);

        float rotation_step = (Mathf.PI * 2f) / num_spokes;
        Vector2 arm = Vector2.zero;
        float radius = pivot.localPosition.x;
        arm.Set(radius, 0);
        carriages.Add(pivot);
        

        for (int i = 1; i < num_spokes; ++i) {
            SetRotationVector2(ref arm, rotation_step);

            Transform t = Instantiate(pivot);
            t.SetParent(transform);
            t.localPosition = new Vector3(arm.x, arm.y, 0f);
            carriages.Add(t);
        }
    }

    private void AdjustCarriages() {
        foreach ( Transform t in carriages ) {
            t.rotation = Quaternion.identity;
        }
    }

    // Update is called once per frame
    void Update () {
        euler_rotation.z = rot_velocity * Time.deltaTime;
        transform.Rotate(euler_rotation);
        AdjustCarriages();
	}
}
