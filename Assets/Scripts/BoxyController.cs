﻿/***
 * 
 * @copyright Copyright 2015 Jeff Cochran
 * 
 * This file is a part of the Jump Cat project.
 * 
 * All rights reserved.
 * 
 * @author mercurythewhite@gmail.com
 * 
 ***/

//#define JUMP_DEBUGGING
#define VELOCITY_DEBUGGING

using UnityEngine;

public class BoxyController : MonoBehaviour {

	private const string kTag = "BoxyController: ";

	// ******************* Physics ************ //

	private Rigidbody2D rigid_body;

	//************ Move Speed *************//

	//! TODO: Set these to const once it's decided on
	public float kMaxSpeed; //!< Ceiling of how fast we go
	public float kSpeedIncrement; //!< How much faster we get per FixedUpdate. This'll make us slide-y
	public float kAirSpeed; //!< Don't speed up like running. Just apply a constant vector 
	private float kHalfAirSpeed; //!< For changing direction midair
	public float kSpeedDamping;

	// ************ Grounding *********** //

	private bool is_grounded = false;
	public EdgeCollider2D ground_checker; //!< for checking if we're on the ground
	public LayerMask ground_layers;

	// ************ Jumping ************ //

	private uint num_jumps;
	public uint max_num_jumps;
	public float jump_force;
	private float diag_jump_force;
	private bool should_do_jump = false;
	public float cling_jump_force;
	public float kClingJumpXDisplacement = 0.1f;

	// ************ Clinging *********** //

	private bool is_clinging = false;
	public LayerMask clingable_layers;
	public EdgeCollider2D right_collider;
	public EdgeCollider2D left_collider;
	public float kDragSpeed;
	private bool is_cling_right = false;

	// ************ Collision Checking ************ //

	private BoxCollider2D cv;

	// Use this for initialization
	void Start () {
		num_jumps = max_num_jumps;
		kHalfAirSpeed = kAirSpeed / 2f;
		Debug.Assert (ground_checker, kTag + "Ground checker not attached!");
		Debug.Assert (right_collider, kTag + "Right collider not attached!");
		Debug.Assert (left_collider, kTag + "Left collider not attached!");

        diag_jump_force = jump_force * Mathf.Cos(Mathf.PI / 4f);
	}

	// FYI: the reference script initializes its references in Awake(), not Start...
	// when does this happen in the lifecycle?
	void Awake() {
		rigid_body = GetComponent<Rigidbody2D> ();
		Debug.Assert (rigid_body, kTag + "unable to find rigid body!");
		cv = GetComponent<BoxCollider2D> ();
		Debug.Assert (cv, kTag + "unable to find box collider!");
	}
	
	// Update is called once per frame
	// called at a different rate than the physics system
	// Called before rendering, and before animations are calculated.
	// You can find delta time via Time.DeltaTime or soemthing
	void Update () {

		if (Input.GetButtonDown ("Jump") && num_jumps > 0 && !should_do_jump) {
			should_do_jump = true;
		}

		if (is_clinging) {
			HandleMovementForCling();
		} else {
			HandleMovementNormal();
		}
	}

	/**
	 * Draw the velocity of the player
	 **/
	private void DebugDrawVelocity() {
		Vector2 velocity;
		if (!Debug.isDebugBuild)
			return;
		velocity.x = -rigid_body.velocity.x * 0.1f;
		velocity.y = -rigid_body.velocity.y * 0.1f;

		velocity.x += transform.position.x;
		velocity.y += transform.position.y;
		Debug.DrawLine (velocity, rigid_body.position);
	}

	private Vector2 cling_jump_vector = new Vector2 ();
	/**
	 * Figure out which way we should jump
	 * return true if we did a jump
	 **/
	private bool DoClingJump() {
		bool left = Input.GetButton ("Left");
		bool right = Input.GetButton ("Right");
		bool up = Input.GetButton ("Up");
		bool down = Input.GetButton ("Down");
	
		should_do_jump = false;

#if WITH_OLD_JUMP_CONTROLS
		if ((!left && !right && !up && !down) ||
		    (!left && !right && down)) { // drop
#else 
		if (!left && !right && down) {
#endif
			cling_jump_vector.x = rigid_body.position.x + ((is_cling_right) ? -kClingJumpXDisplacement : kClingJumpXDisplacement);
			cling_jump_vector.y = rigid_body.position.y;
			rigid_body.MovePosition(cling_jump_vector);
#if JUMP_DEBUGGING
			Debug.Log(kTag+ "Drop\n");
#endif
			// XXX: should dropping cost a jump?
			return true;
		}

		if (!left && !right && up) { // jump straight up
			cling_jump_vector.x = 0f;
			cling_jump_vector.y = jump_force;
			#if JUMP_DEBUGGING
			Debug.Log(kTag+ "Straight up\n");
			#endif
		} else if ((left || right) && !down && !up) { // jump away or do nothing
			if ((is_cling_right && right) ||
				(!is_cling_right && left)) {
				#if JUMP_DEBUGGING
				Debug.Log(kTag+ "Towards diagonal\n");
				#endif
				cling_jump_vector.x = (is_cling_right) ? -diag_jump_force : diag_jump_force;
				cling_jump_vector.y = diag_jump_force;
			} else {
				cling_jump_vector.x = (is_cling_right) ? -cling_jump_force : cling_jump_force;
				cling_jump_vector.y = 0f;
				#if JUMP_DEBUGGING
				Debug.Log(kTag+ "jump_away\n");
				#endif
			}
		} else { // diagonal jump
			if ((is_cling_right && right) ||
			    (!is_cling_right && left)) {
					cling_jump_vector.x = 0f;
					cling_jump_vector.y = jump_force;
					#if JUMP_DEBUGGING
					Debug.Log(kTag+ "Tiny Up: " + cling_jump_vector.ToString() + "\n");
					#endif
			} else {
				cling_jump_vector.x = (is_cling_right) ? -diag_jump_force : diag_jump_force;
				cling_jump_vector.y = (up || (!right && !left)) ? diag_jump_force : -diag_jump_force;
				#if JUMP_DEBUGGING
					Debug.Log(kTag+ "Diagonal: " + cling_jump_vector.ToString() + "\n");
				#endif
			}
		}

		rigid_body.isKinematic = false;
		rigid_body.velocity = cling_jump_vector;
		is_clinging = false;
		--num_jumps;
		return true;
	}

	/**
	 * Handles movement when we're clinging to stuff.
	 **/
	private void HandleMovementForCling() {

		// handle jump
		// if DoClingJump returns false, then we should keep dragging
		if (should_do_jump && DoClingJump()) { 
			return;
		}

        // handle drag
        transform.localPosition = new Vector3( transform.position.x,
        			                           transform.position.y - (kDragSpeed * Time.deltaTime * 1000f),
        			                           transform.position.z );
	}

	private Vector2 hmn_velocity = new Vector2();
	/**
	 * Handles movement when we're not clinging to stuff.
	 **/
	private void HandleMovementNormal() {
		hmn_velocity = rigid_body.velocity;

		if (should_do_jump) {
			should_do_jump = false;
			#if JUMP_DEBUGGING
			Debug.Log(kTag + "Jumpen!\n", this);
			#endif
			hmn_velocity.y = jump_force;
			--num_jumps;
		}

		float move = Input.GetAxis ( "Horizontal" );

        if (is_grounded) {
            if (move != 0f) {
                hmn_velocity.x += (move * kSpeedIncrement);
                if (Mathf.Abs(hmn_velocity.x) > kMaxSpeed) {
                    hmn_velocity.x = (hmn_velocity.x > 0) ? kMaxSpeed : -kMaxSpeed;
                }
            } else if (hmn_velocity.x != 0f) {
                if (Mathf.Abs(hmn_velocity.x) <= 0.01f) {
                    hmn_velocity.x = 0f;
                } else {
                    hmn_velocity.x -= kSpeedDamping * Time.deltaTime * 1000f * hmn_velocity.x;
                }
            }
        } else { // airborne
			if (move != 0f) {
				if (Mathf.Abs (hmn_velocity.x) > kAirSpeed) {
					if ((hmn_velocity.x * move) < 0f) {
						hmn_velocity.x += move * kHalfAirSpeed * 1000f * Time.deltaTime;
					}
				} else {
					hmn_velocity.x = move * kAirSpeed;
				}
			}
		}

		bool lc_touching_noncling = left_collider.IsTouchingLayers (ground_layers.value & ~clingable_layers.value);
		bool rc_touching_noncling = right_collider.IsTouchingLayers (ground_layers.value & ~clingable_layers.value);

		// don't press into walls
		if ( lc_touching_noncling || rc_touching_noncling ) {

			if ((lc_touching_noncling && hmn_velocity.x < 0f) ||
			    (rc_touching_noncling && hmn_velocity.x > 0f)) {
				hmn_velocity.x = 0f;
			}
		}

		// apply velocity finally. Keep this down at the bottom
		rigid_body.velocity = hmn_velocity;
	}

    bool is_child_of_moving_terrain = false;
    int moving_terrain_parent_id = -1;
    void OnCollisionEnter2D(Collision2D collision) {
        if ( !is_child_of_moving_terrain && 
             collision.gameObject.tag == "MovingTerrain" &&
             collision.collider.IsTouching(ground_checker) ) {
            moving_terrain_parent_id = collision.gameObject.GetInstanceID();
            transform.SetParent(collision.gameObject.transform, true);
            is_child_of_moving_terrain = true;
        }
    }

    void OnCollisionExit2D(Collision2D collision) {
        if ( is_child_of_moving_terrain &&
             moving_terrain_parent_id == collision.gameObject.GetInstanceID() ) {
             transform.SetParent(null, true);
             is_child_of_moving_terrain = false;
        }
    }


    // Updated in lockstep with the physics code, which is of a 
    // different frequency than the regular update code.
    // If you're trying to do something that will impact 
    // physics calculations, it should happen here.
    // !! There's no need to multiply by delta time because fixedupdate is
    // LITERALLY FIXED. It will be called at regular intervals, 
    // so no scaling needs to apply
    void FixedUpdate() {

		bool was_grounded = is_grounded;

		is_grounded = ground_checker.IsTouchingLayers (ground_layers.value);

		if (was_grounded != is_grounded) {
			if (is_grounded) {
#if JUMP_DEBUGGING
				Debug.Log(kTag + "Landed!\n", this);
#endif
				num_jumps = max_num_jumps;
			} else {
#if JUMP_DEBUGGING
				Debug.Log(kTag + "Liftoff!\n", this);
#endif
			}
        }


        bool was_clinging = is_clinging;
		bool cling_dir_right = false;

		is_clinging = false;
		if ((!is_grounded && !was_clinging && cv.IsTouchingLayers (clingable_layers.value)) ||
		    was_clinging) {
			is_clinging = (cling_dir_right = right_collider.IsTouchingLayers(clingable_layers.value)) || 
						  left_collider.IsTouchingLayers(clingable_layers.value);
			if ( is_clinging && rigid_body.velocity.y > 0f &&
			    ((cling_dir_right && rigid_body.velocity.x <= 0f) || 
			    (!cling_dir_right && rigid_body.velocity.x >= 0f))){
				is_clinging = false;
			}
		}

		if (is_clinging && !was_clinging) {
			rigid_body.isKinematic = true;
			is_cling_right = cling_dir_right;
			num_jumps = max_num_jumps;
#if JUMP_DEBUGGING
			Debug.Log(kTag+"Become Kinematic!");
#endif
		} else if ((!is_clinging && was_clinging) || 
		           (is_grounded && !was_grounded)) {
			is_clinging = false;
			rigid_body.isKinematic = false;
#if JUMP_DEBUGGING
			Debug.Log(kTag+"Become Dynamic!");
#endif
		}

		if (Debug.isDebugBuild) {
#if VELOCITY_DEBUGGING
			DebugDrawVelocity ();
#endif
		}
	}
}
