/***
 * 
 * @copyright Copyright 2015 Jeff Cochran
 * 
 * This file is a part of the Jump Cat project.
 * 
 * All rights reserved.
 * 
 * @author mercurythewhite@gmail.com
 * 
 ***/

using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class Camera2DFollowDumb : MonoBehaviour
    {
        public Transform target;
		public float vertical_offset;
		private Vector3 tmp_vector;

        // Use this for initialization
        private void Start()
        {
			tmp_vector = new Vector3 ();
			tmp_vector.z = transform.position.z;
        }

        // Update is called once per frame
        private void Update()
        {
			tmp_vector.x = target.position.x;
			tmp_vector.y = target.position.y;// + vertical_offset;
			transform.position = tmp_vector;
        }
    }
}
