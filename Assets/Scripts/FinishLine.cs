﻿/***
 * 
 * @copyright Copyright 2015 Jeff Cochran
 * 
 * This file is a part of the Jump Cat project.
 * 
 * All rights reserved.
 * 
 * @author mercurythewhite@gmail.com
 * 
 ***/

using UnityEngine;

public class FinishLine : MonoBehaviour {
	private const string kTag = "FinishLine: ";

	private AudioSource src;
	private bool has_triggered = false;
    public int next_level_index;

	void Start() {
		src = GetComponent<AudioSource> ();
		Debug.Assert (src, kTag + "Couldn't find audio source!");
	}

	private void OnTriggerEnter2D (Collider2D other) {
        if (has_triggered)
            return;

		if (other.tag == "Player") {
			src.Play();
            GameObject boxanne = GameObject.FindWithTag("Player");
            Debug.Assert(boxanne, kTag + "Couldn't find player object!");
            boxanne.SetActive(false);
            GameObject camera = GameObject.FindWithTag("MainCamera");
            Debug.Assert(camera, kTag + "Couldn't find main camera!!!!");
            // ugly coupling... blech
            SpriteRenderer rend = camera.GetComponentInChildren<SpriteRenderer>();
            if (null != rend) {
                rend.enabled = true;
            }
            has_triggered = true;
        }
	}

    void Update() {
        // not particularly robust logic. Beware of potential bugs
        if (has_triggered && !src.isPlaying) {
            if (-1 != next_level_index) {
                Application.LoadLevel(next_level_index);
            } 
        }
    }
}